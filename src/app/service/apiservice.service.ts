import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { throwError } from "rxjs";
import { map, catchError } from 'rxjs/operators';
import { Observable } from "rxjs";
import { Subject } from 'rxjs';
import { Location } from '@angular/common';
import * as sha512 from 'js-sha512';
import { CookieService } from 'ngx-cookie-service';




@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  url: any;
  isPrinting = false;
  constructor(private http: HttpClient, private router: Router,private location: Location,private cookieService: CookieService) { }
  private emitChangeSource = new Subject<any>();

  changeEmitted$ = this.emitChangeSource.asObservable();

  emitChange(data: {}) {
    this.emitChangeSource.next(data);
  }

  

  back() {
    this.location.back();
  }

  geturl() {
    return 'https://www.google.com/'
  }

  getuserinfo(){
    return  JSON.parse(localStorage.getItem('userInfo'));
  }

  gettoken(){
    return localStorage.getItem('accessToken');
  }

  

  apigetcall(url, params) {
    this.url = environment.apiurl + url;
    return this.http.get<any>(this.url, params).pipe(map((responses: any) => {
      return responses
    }), catchError((err) => {
      return throwError(err)
      // Do messaging and error handling here
    }));
  }


  
  apipostcall(url, params) {
    console.log("post call data:", url, params)
    this.url = environment.apiurl + url;


    return this.http.post<any>(this.url, params).pipe(map((responses: any) => {
      return responses
    }), catchError((err) => {
      return throwError(err)
      // Do messaging and error handling here
    }));
  }

  apideletecall(url, params) {
    console.log("delete call data:", url, params)
    this.url = environment.apiurl + url;
    return this.http.delete<any>(this.url, params).pipe(map((responses: any) => {
      return responses
    }), catchError((err) => {
      return throwError(err)
      // Do messaging and error handling here
    }));
  }

  apiputcall(url, params) {
    console.log("put call data:", url, params)
    this.url = environment.apiurl + url;
    return this.http.put<any>(this.url, params).pipe(map((responses: any) => {
      return responses
    }), catchError((err) => {
      return throwError(err)
      // Do messaging and error handling here
    }));
  }

  login(user: any) {
    let URL = environment.apiurl+"users/login";
    let sendParam = {
        "email": user.email,
        "password": sha512.sha512(user.password)
    };
    console.log(sendParam.password)
    return this.http.post<any>(URL,sendParam).pipe(map(response => {
        this.cookieService.set( 'accessToken', response.data.access_token );
     
        localStorage.setItem('accessToken', response.data.access_token);
        localStorage.setItem('userInfo', JSON.stringify(response));
        return response;
    }),catchError((err) => {
       return throwError(err);
   }));
  }
}
