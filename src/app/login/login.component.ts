import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../service/apiservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  paramsdata: any = {
    email: 'tamilselvan@mailinator.com',
    password: 'Staller123#'
  }
  constructor(public apiservice: ApiserviceService, public actroute: ActivatedRoute, public router: Router, private cookieService: CookieService) { }

  ngOnInit() {
    this.actroute.paramMap.subscribe(data => {
      let authtoken=data.get("authtoken");
      if(authtoken){
        this.cookieService.set('accessToken',data.get("authtoken"));
      }
    });
    let accessToken = this.cookieService.get('accessToken');
    if (accessToken) {
      this.getuser()
    }
    else {
      this.geturl()
    }
  }
	
  getuser() {
    this.apiservice.apigetcall('employees', {}).subscribe(results => {
      this.router.navigate(['/horses']);
    },
      (err) => {
        // this.toastr.error(err.error.error, 'Error');
      });
  }

  geturl() {
    window.location.href = this.apiservice.geturl()
  }

  login(form) {
    if (form.valid) {
      this.apiservice.login(this.paramsdata).subscribe(results => {
        this.router.navigate(['/horses']);
      },
        (err) => {
          // this.toastr.error(err.error.error, 'Error');
        });
    }
  }

}
