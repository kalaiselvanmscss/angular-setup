import { Component, OnInit } from '@angular/core';
import { ApiserviceService} from '../service/apiservice.service'
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public samplePagesCollapsed = true;
  userinfo:any
  constructor(public apiservice:ApiserviceService) { }

  ngOnInit() {
    this.userinfo=this.apiservice.getuserinfo()
    console.log("userinfo",this.userinfo)
  }

}
