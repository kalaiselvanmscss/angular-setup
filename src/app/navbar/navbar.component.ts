import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';

import {Router} from "@angular/router"
import { ApiserviceService} from '../service/apiservice.service'


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [NgbDropdownConfig]
})
export class NavbarComponent implements OnInit {
  public sidebarOpened = false;
  userinfo:any
  toggleOffcanvas() {
    this.sidebarOpened = !this.sidebarOpened;
    if (this.sidebarOpened) {
      document.querySelector('.sidebar-offcanvas').classList.add('active');
    }
    else {
      document.querySelector('.sidebar-offcanvas').classList.remove('active');
    }
  }
  constructor(config: NgbDropdownConfig,private router:Router,public apiservice:ApiserviceService) {
    config.placement = 'bottom-right';
  }
  ngOnInit() {
    this.userinfo=this.apiservice.getuserinfo()
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  

}
