import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { ApiserviceService } from '../service/apiservice.service';
import { Router, ActivatedRoute } from '@angular/router';


import { Subject } from 'rxjs';
import Swal from 'sweetalert2'



@Component({
  selector: 'app-horse',
  templateUrl: './horse.component.html',
  styleUrls: ['./horse.component.scss']
})
export class HorseComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: any = new Subject();
  constructor(public apiservice: ApiserviceService, private router: Router, public route: ActivatedRoute ) { }

  ngOnInit() {
    var self = this;
    this.dtOptions = {
      ajax: (dataTablesParameters: any, callback) => {
        let endpoint = "horses";
        this.apiservice.apigetcall(endpoint, dataTablesParameters).subscribe(resp => {
          callback({
            data: resp.data
          });
        });
      },
      columns: [{
        title: 'S.NO',
        data: 'horse_name'
      },
      {
        title: 'Horse Name',
        data: 'horse_name'
      },
      {
        title: 'Horse Number',
        data: 'horse_number'
      },{
        title: 'Aqha Number',
        data: 'aqha_number'
      },{
        title: 'Color',
        data: 'color'
      },{
        title: 'Actions', orderable: false, render: function (data: any, type: any, full: any) {

          let tHtml = '<p class="mb-0"><a class="btn-view"><i class="fa fa-eye"></i></a><a class="btn-edit"><i class="fa fa-pencil"></i></a><a class="btn-delete"><i class="fa fa-trash"></i></a></p>'

          return tHtml;
        }
      }],
      rowCallback: (row: Node, data: any | Object, index: number) => {
        const self = this;
        let countindex:any = index + 1
        $('td:eq(0)', row).html(countindex);
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        $('td', row).unbind('click');
        $('.btn-edit', row).bind('click', () => {

          this.router.navigate(['horses/edit/'+data.id])
        });
        $('.btn-delete', row).bind('click', () => {
          self.deleteRecord(data);
        });
        return row;
      }
    };
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  deleteRecord(clickObj) {
    Swal.fire({
      title: 'Delete?',
      text: 'Do you want to continue',
      type: 'info',
      confirmButtonText: 'Okay'
    }).then((isConfirm) => {
      if (isConfirm.value == true) {
        this.apiservice.apideletecall('horses/' + clickObj.id, {}).subscribe(resp => {
          // this.toastr.success('The Selected Record is Deleted!', 'Success');
          setTimeout(() => {
            this.rerender();
          }, 300);
        });
      }
    }), (error: any) => { 
       // this.toastr.error(error.error, 'Error');
    }
  }



}
