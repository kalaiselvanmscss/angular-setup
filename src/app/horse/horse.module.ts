import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HorseComponent } from './horse.component';
import { AddeditHorseComponent } from './addedit-horse/addedit-horse.component';
import { DataTablesModule } from 'angular-datatables';



const routes: Routes = [
  { path: '', component: HorseComponent },
  { path: 'add', component: AddeditHorseComponent },
  { path: 'edit/:id', component: AddeditHorseComponent }
];

@NgModule({
  declarations: [HorseComponent, AddeditHorseComponent],
  imports: [
    CommonModule,
    DataTablesModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class HorseModule { }
