import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../../service/apiservice.service';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-addedit-horse',
  templateUrl: './addedit-horse.component.html',
  styleUrls: ['./addedit-horse.component.scss']
})
export class AddeditHorseComponent implements OnInit {
  paramsdata: any = {}
  submitted: boolean = false
  formfield: any = []
  constructor(public apiservice: ApiserviceService, private router: Router, public route: ActivatedRoute, ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(data => {
      this.paramsdata.id = data.get("id");
    });
    if (this.paramsdata.id != undefined) {
      this.apiservice.apigetcall('horses/' + this.paramsdata.id, {}).subscribe(resp => {
        this.paramsdata = resp.data
      });
    }
    let formfield = []
    for (const key in this.paramsdata) {
      if (this.paramsdata.hasOwnProperty(key)) {
        // const element = this.paramsdata[key];
        formfield.push({ key: key, value: this.paramsdata[key], error: '' })
      }
    }
    this.formfield = formfield;
  }

  upload(event:any){
    // console.log("file",event.target.value,"\n ",event.target)
    this.paramsdata.image = <File>event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      
    }
   
  }

  addeditsave(form) {
    console.log(form)
    this.submitted=true
    if (form.valid) {
      const formData = new FormData();
      formData.append('horse_name',this.paramsdata.horse_name);
      formData.append('horse_number',this.paramsdata.horse_number);
      formData.append('image',this.paramsdata.image);
      formData.append('aqha_number',this.paramsdata.aqha_number);
      formData.append('color',this.paramsdata.color);
      if (this.paramsdata.id == undefined) {
        this.apiservice.apipostcall('horses', formData).subscribe(resp => {
          this.router.navigate(['horses'])
          this.submitted=false
        },
          (err) => {
            this.submitted=false
          });
      }
      else {
        this.apiservice.apiputcall('horses/' + this.paramsdata.id, formData).subscribe(resp => {
          this.router.navigate(['horses'])
          this.submitted=false
        },
          (err) => {
            this.submitted=false
          });
      }
    }
  }

}
