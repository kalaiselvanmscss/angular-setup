import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditHorseComponent } from './addedit-horse.component';

describe('AddeditHorseComponent', () => {
  let component: AddeditHorseComponent;
  let fixture: ComponentFixture<AddeditHorseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditHorseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditHorseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
