import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, filter, tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private cookieService: CookieService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // this._loadingBar.start();

        // add authorization header with jwt token if available
        //let accessToken = localStorage.getItem('accessToken');
        let accessToken = this.cookieService.get('accessToken');
        if (accessToken) {
              request = request.clone({
                  setHeaders: {
                    Authorization: `Bearer ${accessToken}`
                  }
              });
        }
        const started = Date.now();
        return next.handle(request).pipe( tap(event => {
            if (event instanceof HttpResponse) {
                const elapsed = Date.now() - started;
                console.log(`Request for ${request.urlWithParams} took ${elapsed} ms.`);
              }
            }, error => {
                console.error('NICE ERROR', error)
            }));
    }
}
