import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { ArchwizardModule } from 'angular-archwizard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';

import { MenulayoutComponent } from './layout/menulayout/menulayout.component';
import { FulllayoutComponent } from './layout/fulllayout/fulllayout.component';
import { LoginComponent } from './login/login.component';

import {NavbarComponent} from './navbar/navbar.component'
import {SidebarComponent} from './sidebar/sidebar.component'

import { AuthGuard } from './auth/auth.guard';
import { JwtInterceptor } from './auth/jwt.interceptor';
import { ApiserviceService } from './service/apiservice.service';
import { CookieService } from 'ngx-cookie-service';

import { from } from 'rxjs';


@NgModule({
  declarations: [
    AppComponent,
    MenulayoutComponent,
    FulllayoutComponent,
    NavbarComponent,
    SidebarComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    NgbModule.forRoot(),
    ArchwizardModule,
    HttpClientModule
  ],
  providers: [
    ApiserviceService,
    AuthGuard,
    CookieService,
    {
       provide: HTTP_INTERCEPTORS,
       useClass: JwtInterceptor,
       multi: true
   }],
  bootstrap: [AppComponent]
})
export class AppModule { }