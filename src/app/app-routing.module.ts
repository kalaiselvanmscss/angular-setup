import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { FulllayoutComponent } from './layout/fulllayout/fulllayout.component';
import { MenulayoutComponent } from './layout/menulayout/menulayout.component';
import { AuthGuard } from './auth/auth.guard';




const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: '',
    component: FulllayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'login/:authtoken',
        component: LoginComponent
      }
    ]
  },
  {
    path: '',
    component: MenulayoutComponent,
    canActivate:[AuthGuard],
    children: [
      { path: 'horses', loadChildren: './horse/horse.module#HorseModule' },
    ]
  },
  { path: "**", redirectTo: "login" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
