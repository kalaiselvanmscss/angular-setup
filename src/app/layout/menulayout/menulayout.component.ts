import { Component, OnInit } from '@angular/core';
import {
  Router, NavigationStart, NavigationCancel, NavigationEnd,NavigationError
} from '@angular/router';

@Component({
  selector: 'app-menulayout',
  templateUrl: './menulayout.component.html',
  styleUrls: ['./menulayout.component.scss']
})
export class MenulayoutComponent implements OnInit {
  loading:boolean
  constructor(private router: Router) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationStart) {
          this.loading = true;
        }
        else if (
          event instanceof NavigationEnd ||
          event instanceof NavigationCancel || event instanceof NavigationError
        ) {
          setTimeout(() => {
            this.loading = false;
          }, 300);

        }
      });
  }

}
